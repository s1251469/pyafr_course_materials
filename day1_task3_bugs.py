## This script has bugs! Save a new copy and try and fix the bugs in it.

import iris
import matplotlib.pyplot as plt
import numpy as np
import iris.coord_categorisation # allows year/month/hour auxillary coordinates to be added

cube_pres = iris.load_cube('./data_all/pr_Amon_*_historical_*.nc')
cube_fut = iris.load_cube('./data_all/pr_Amon_HadGEM2-ES_rcp85_r1i1p1_203012-205511.nc')

## change units from kg m-2 s-1 to mm hr-1
cube_pres.data = cube_pres.data *3600.0
cube_pres.units = "mm hr-1"
cube_fut.data = cube_fut.data *3600.0
cube_fut.units = "mm hr-1"


## select East Africa
## set longitude range
min_lon = 20.0; max_lon = 52.0
def lon_range(cell):
	return min_lon <= cell <= max_lon
min_lat = -10.0; max_lat = 20.0
def lat_range(cell):
	return min_lat <= cell <= max_lat
constraint_EastAfrica = iris.Constraint(longitude=lon_range, latitude=lat_range)
## extract EA region
cube_pres_EA = cube_pres.extract(constraint_EA)
cube_fut_EA = cube_fut.extract(constraint_EA)

## take a spatial mean (not worrying about area-weighting but we could)
cube_pres_EA_ts = cube_pres_EA.collapsed(['longitude','latitude'], iris.analysis.MEAN)
cube_fut_EA_ts = cube_fut_EA.collapsed(['longitude','latitude'], iris.analysis.MEAN)
## calculate the year mean time series from month mean
iris.coord_categorisation.add_year(cube_pres_EA_ts, 'time')
cube_pres_ts_yr = cube_pres_EA_ts.aggregated_by('year',iris.analysis.MEAN)
iris.coord_categorisation.add_year(cube_fut_EA_ts, 'time')
cube_fut_ts_yr = cube_fut_EA_ts.aggregated_by('year',iris.analysis.MEAN)


# take a temporal mean
cube_pres_EA_2D = cube_pres_EA.collapsed(['time'], iris.analysis.MEAN)
cube_fut_EA_2D = cube_fut_EA.collapsed(['time'], iris.analysis.MEAN)

# calculate the difference
diff_EA_2D = cube_fut_EA_2D - cube_pres_EA_2D


## plotting
fig = plt.figure(figsize=(12,8))
# plot of temperal change
ax1 = qplt.subplot(1,2,1)
qplt.plot(cube_pres_EA_ts, color='grey')
qplt.plot(cube_pres_ts_yr, color='black')
qplt.plot(cube_fut_EA_ts,color='pink')
qplt.plot(cube_fut_ts_yr,color='darkred')
plt.title('HadGEM2-ES')


# plot of change in spatial distribution
ax2 = plt.subplot(1,1,2)
qplt.contourf(diff_EA_2D, cmap=plt.cm.RdBu, levels=np.arange(-0.026,0.027,0.004), extend='both')
plt.gca().coastlines()
plt.title('HadGEM2-ES 2040s RCP8.5 - present day')

## SHOW
#qplt.show()

## SAVE
## in git bash use >>> start test.png
plt.savefig("day1_task3.png")
