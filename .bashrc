# BASHRC script: Declan Finney 13/03/2018
# this script is run every time you open a terminal.

# add anaconda and scripts to path
# NOTE/ if this doesn't work, it might be because
# the Anaconda directory is not installed in your
# $HOME directory. In which case replace $HOME with
# the correct path.
export PATH=$PATH:$HOME/Anaconda2:$HOME/Anaconda2/Scripts

# need to use winpty to open python on windows git bash
alias ipython='winpty ipython'
alias python='winpty python'
