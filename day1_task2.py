# import the modules we want
import iris # this is to read in data and analyse it
import iris.quickplot as qplt # to plot the data easily

# read in data
cube = iris.load_cube('./data/tas_Amon_HadGEM2-ES_historical_r1i1p1_198412-200511.nc')

# take time average
cube_mean = cube.collapsed('time', iris.analysis.MEAN)

# plot the data
qplt.contourf(cube_mean)
# Extra:add coastlines
print("Adding coastlines this time too!")
import matplotlib.pyplot as plt
plt.gca().coastlines()

## To save instead, uncomment line below.
plt.savefig('day1_task2.png')
## To show the figure uncomment the line below.
qplt.show()

## now close it to remove from python's plot memory
plt.close()
