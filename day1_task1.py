# import modules (these contain functions data analysis and plotting functions we want to use)
import netCDF4 as nc4
import numpy as np # allows us to work with arrays of data
import matplotlib.pyplot as plt # alows us to plot data

# read in historical rainfall model data
f = nc4.Dataset('./data/tas_Amon_HadGEM2-ES_historical_r1i1p1_198412-200511.nc', 'r') ## filename and 'r' means "to read" 
# we want the latitude and longitude values of the points in the data
lats = f.variables['lat'][:]
lons = f.variables['lon'][:]
temp = f.variables['tas'][:]

# time average the data (this will give a map of average rainfall)
data_mean = np.mean(temp, axis=0) # the zero axis is the time axis we want to average over

# plot the data
plt.contourf(lons, lats, data_mean)
plt.show()

