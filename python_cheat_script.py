## This script is a reference I put together of some 
## key things you'll want to know about python to 
## do your analysis.

# 1) The first element in the array is 0 element
# 2) Tabs are important. They tell python 
#    loops/conditions run to

#### LISTS ####
xlist = [1,22,333,4444]
print(xlist[0])   # is 1
print(xlist[1])   # is 22
print(xlist[-1])  # is 4444
print(xlist[3])   # is 4444
print(xlist[0:3]) # is [1,22,333]

#### FOR LOOPS ####
for i in xlist:
	print(i)
print("finished loop. No longer in tab")

#### IF STATEMENT ####
if xlist[0]==5:
	print("Yay!")
elif xlist[1]==22:
	print("ok")
else:
	print("nay")

#### FUNCTION ####
def squared(number):
	return number*number

print("four squared is ",squared(4))
print(xlist[2]," squared is ",squared(xlist[2]))

#### LIST COMPREHENSION ####
## this is a neat way to apply functions. Format::
# [<expression> for <expr> in <sequence> if <condition>]
print(range(4),[x**2 for x in range(4) if x>=2])

